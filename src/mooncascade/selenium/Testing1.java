package mooncascade.selenium;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Testing1 {
private static String URL = "https://www.gymwolf.com/staging";
private static String TCEmail = "sandrak@mooncascade.com";
private static String TCPassword = "mooncascade";

	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = setupDriver();
		driver.findElement(By.xpath(Elements.loginButton)).click();
		driver.findElement(By.xpath(Elements.Email)).click();
		driver.findElement(By.xpath(Elements.Email)).sendKeys(TCEmail);
		driver.findElement(By.xpath(Elements.Password)).click();
		driver.findElement(By.xpath(Elements.Password)).sendKeys("TCPassword");
		Thread.sleep(5000);
		driver.findElement(By.xpath(Elements.signInButton)).click();
	}
	private static WebDriver setupDriver() {	
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Waldrak\\Downloads\\Selenium\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get(URL);
		driver.manage().window().maximize();
		//Waits 10 seconds until it finds the correct element. 
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		return driver;
	}
}
