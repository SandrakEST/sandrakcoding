package mooncascade.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

//Moving/navigating through the navbar. 
public class Testing2 {

private static String URL = "https://www.gymwolf.com/staging";
	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = setupDriver();
		driver.findElement(By.xpath(Elements.nav1)).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath(Elements.nav2)).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath(Elements.nav3)).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath(Elements.nav4)).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath(Elements.nav5)).click();
		
}
	private static WebDriver setupDriver() {	
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Waldrak\\Downloads\\Selenium\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get(URL);
		driver.manage().window().maximize();
		return driver;
	}
}